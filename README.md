# Instructions

```bash
# Test (using in-memory H2 database)
$ ./gradlew test

# Run (using external postgres database)
$ docker run --name psql -e POSTGRES_PASSWORD=helloworld -e POSTGRES_DB=booker -d -p 5432:5432 postgres
$ ./gradlew bootRun
```

# HTTP API
```bash
# Create booking
$ curl -XPOST -H 'Content-Type: application/json' -d '{"room":1,"startAt":"2023-02-17 10:00:00","endAt":"2023-02-17 12:00:00"}' localhost:8080/bookings

# List bookings
$ curl localhost:8080/bookings

# Delete a booking
$ curl -XDELETE localhost:8080/bookings/1
```