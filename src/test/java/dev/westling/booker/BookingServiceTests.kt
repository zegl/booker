package dev.westling.booker

import io.kotest.assertions.throwables.shouldThrow
import io.kotest.core.spec.style.FunSpec
import io.kotest.datatest.withData
import io.kotest.extensions.spring.SpringExtension
import io.kotest.matchers.should
import io.kotest.matchers.shouldBe
import io.kotest.matchers.string.startWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import java.lang.IllegalArgumentException
import java.time.LocalDateTime
import java.time.temporal.ChronoUnit

data class OverlapTestData(val start: Int, val end: Int, val expected: Boolean)

@SpringBootTest
class BookingServiceTests : FunSpec() {
    override fun extensions() = listOf(SpringExtension)

    @Autowired
    private lateinit var bookingService: BookingService

    @Autowired
    private lateinit var bookingRepository: BookingRepository

    init {
        this.context("test overlapping bookings") {
            withData(
                OverlapTestData(9, 10, true), // within existing
                OverlapTestData(10, 13, true), // starts within existing
                OverlapTestData(7, 9, true), // ends within existing
                OverlapTestData(20, 22, true), // equal to existing
                OverlapTestData(7, 12, true), // wraps existing

                OverlapTestData(8, 10, true), // same start, ends within
                OverlapTestData(8, 12, true), // same start, ends after

                OverlapTestData(7, 11, true), // same end, starts before
                OverlapTestData(9, 11, true), // same end, starts within

                OverlapTestData(15, 17, false), // not overlapping
                OverlapTestData(11, 13, false), // not overlapping, touching start
                OverlapTestData(7, 8, false), // not overlapping, touching end

                OverlapTestData(11, 20, false), // not overlapping, touching end and start of different existing

            ) { (start, end, expected) ->
                val existing = listOf(
                    Booking(room = 1, startAt = time(8), endAt = time(11)),
                    Booking(room = 1, startAt = time(20), endAt = time(22)),
                )
                anyBookingOverlaps(existing, time(start), time(end)) shouldBe expected
            }
        }

        this.context("BookingService") {
            beforeEach {
                bookingRepository.deleteAll()
            }

            test("make booking") {
                bookingService.reserve(1, time(8), time(11))
                bookingService.listAll().count() shouldBe 1
            }


            test("booking starting before 7 throws") {
                val e = shouldThrow<IllegalArgumentException> {
                    bookingService.reserve(1, time(6, 59), time(12))
                }
                e.message should startWith("Invalid booking start time")
            }

            test("booking end at after 22 throws") {
                val e = shouldThrow<IllegalArgumentException> {
                    bookingService.reserve(1, time(12), time(22, 1))
                }
                e.message should startWith("Invalid booking end time")
            }

            test("book end before start") {
                val e = shouldThrow<IllegalArgumentException> {
                    bookingService.reserve(1, time(12), time(11))
                }
                e.message should startWith("Invalid start or end time")
            }

            test("reject booking in the past") {
                // yesterday
                val start = LocalDateTime.now().minusDays(1).withHour(12)
                val end = LocalDateTime.now().minusDays(1).withHour(13)

                val e = shouldThrow<IllegalArgumentException> {
                    bookingService.reserve(1, start, end)
                }
                e.message should startWith("Invalid booking start time")
            }

            test("reject invalid room number") {
                val e = shouldThrow<IllegalArgumentException> {
                    bookingService.reserve(3, time(11), time(12))
                }
                e.message should startWith("Invalid room number (3)")
            }

            test("make one reservation and get") {
                bookingService.reserve(1, time(8), time(11))
                bookingService.listAll().count() shouldBe 1
            }

            test("make two reservations and get") {
                val first = bookingService.reserve(1, time(8), time(11))
                bookingService.listAll().count() shouldBe 1

                val second = bookingService.reserve(2, time(8), time(11))

                bookingService.listAll().count() shouldBe 2
                bookingService.get(first.id!!)!!.room shouldBe 1
                bookingService.get(second.id!!)!!.room shouldBe 2
            }

            test("reject overlapping") {
                bookingService.reserve(1, time(8), time(11))

                val e = shouldThrow<BookingException> {
                    bookingService.reserve(1, time(10), time(12))
                }
                e.message should startWith("Booking overlaps with existing booking")
            }

            test("happy path multiple bookings") {
                bookingService.reserve(1, time(7), time(11))
                bookingService.reserve(1, time(11), time(12))
                bookingService.reserve(1, time(13), time(20))
                bookingService.reserve(1, time(20), time(22))

                bookingService.reserve(2, time(8), time(16))
                val b = bookingService.reserve(2, time(16), time(17))
                // delete
                bookingService.delete(b.id!!)
                // book a new one
                bookingService.reserve(2, time(16, 30), time(17, 30))
            }

            test("delete booking") {
                val b = bookingService.reserve(1, time(8), time(11))
                bookingService.delete(b.id!!)
                // assert allowed to make a new booking at the same time
                bookingService.reserve(1, time(8), time(11))
            }
        }
    }
}

/**
 * hour test helper, create a booking for tomorrow at hour
 */
fun time(hour: Int): LocalDateTime {
    return LocalDateTime.now().plusDays(1).withHour(hour).truncatedTo(ChronoUnit.HOURS)
}

fun time(hour: Int, minute: Int): LocalDateTime {
    return LocalDateTime.now().plusDays(1).withHour(hour).withMinute(minute).truncatedTo(ChronoUnit.MINUTES)
}
