package dev.westling.booker

import com.fasterxml.jackson.annotation.JsonFormat
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import java.time.LocalDateTime

data class BookingRequest(
    val room: Int,
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss") val startAt: LocalDateTime,
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss") val endAt: LocalDateTime,
)

data class BookingResponse(
    val id: Long,
    val room: Int,
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss") val startAt: LocalDateTime,
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss") val endAt: LocalDateTime,
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss") val deletedAt: LocalDateTime?,
)

fun Booking.toResponse() = BookingResponse(
    id = id!!,
    room = room,
    startAt = startAt,
    endAt = endAt,
    deletedAt = deletedAt,
)

@RestController
class BookingController @Autowired constructor(private var bookingService: BookingService) {
    @GetMapping("/bookings")
    fun listBookings(): ResponseEntity<List<BookingResponse>> {
        return ResponseEntity.ok(bookingService.listAll().map { it.toResponse() }.toList())
    }

    @PostMapping("/bookings")
    fun createBooking(@RequestBody request: BookingRequest): ResponseEntity<BookingResponse> {
        val booking = bookingService.reserve(request.room, request.startAt, request.endAt)
        return ResponseEntity.ok(booking.toResponse())
    }

    @GetMapping("/bookings/{id}")
    @ResponseBody
    fun getBooking(@PathVariable id: Long): ResponseEntity<BookingResponse> {
        val booking = bookingService.get(id)
        booking?.let {
            return ResponseEntity.ok(booking.toResponse())
        }
        return ResponseEntity.notFound().build()
    }

    @DeleteMapping("/bookings/{id}")
    @ResponseBody
    fun deleteBooking(@PathVariable id: Long): ResponseEntity<BookingResponse> {
        val booking = bookingService.delete(id)
        booking?.let {
            return ResponseEntity.ok(booking.toResponse())
        }
        return ResponseEntity.notFound().build()
    }
}
