package dev.westling.booker

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.repository.findByIdOrNull
import org.springframework.stereotype.Service
import java.time.LocalDateTime
import java.time.temporal.ChronoUnit

class BookingException(message: String) : Exception(message)

@Service
class BookingService @Autowired constructor(
    private var bookingRepository: BookingRepository,
) {
    fun reserve(room: Int, startAt: LocalDateTime, endAt: LocalDateTime): Booking {
        require(validNewBookingTime(startAt)) { "Invalid booking start time ($startAt)" }
        require(validNewBookingTime(endAt)) { "Invalid booking end time ($endAt)" }
        require(endAt.isAfter(startAt)) { "Invalid start or end time" }
        require(startAt.toLocalDate().isEqual(endAt.toLocalDate())) { "Start and end time needs to be on the same day" }
        require(validRoom(room)) { "Invalid room number ($room)" }

        // Get all bookings for the same room during the same day of the proposed booking
        // Reject the new booking if there are overlaps
        val startOfDay = startAt.truncatedTo(ChronoUnit.DAYS)
        val endOfDay = startOfDay.plusDays(1)
        val sameDayBookings =
            bookingRepository.findByRoomAndStartAtBetweenAndDeletedAtIsNull(room, startOfDay, endOfDay)

        if (anyBookingOverlaps(sameDayBookings, startAt, endAt)) {
            throw BookingException("Booking overlaps with existing booking")
        }

        val booking = Booking(room = room, startAt = startAt, endAt = endAt)

        return bookingRepository.save(booking)
    }

    fun listAll(): Iterable<Booking> {
        return bookingRepository.findByDeletedAtIsNull()
    }

    fun get(id: Long): Booking? {
        return bookingRepository.findByIdOrNull(id)
    }

    fun delete(id: Long): Booking? {
        val booking = get(id)
        booking?.let {
            booking.deletedAt = LocalDateTime.now()
            bookingRepository.save(booking)
        }
        return booking
    }
}

fun validNewBookingTime(ts: LocalDateTime): Boolean {
    // reject times in the past
    if (ts.isBefore(LocalDateTime.now())) {
        return false
    }

    // earliest and latest allowed times this day
    val earliest = ts.withHour(7).truncatedTo(ChronoUnit.HOURS)
    val latest = ts.withHour(22).truncatedTo(ChronoUnit.HOURS)

    if (ts.isBefore(earliest)) {
        return false
    }
    if (ts.isAfter(latest)) {
        return false
    }
    return true
}

fun validRoom(room: Int): Boolean {
    return setOf(1, 2).contains(room)
}

/**
 * anyBookingOverlaps checks if any Booking in bookings overlaps with startAt and endAt
 *
 * @param bookings
 * @param startAt
 * @param endAt
 */
fun anyBookingOverlaps(bookings: Iterable<Booking>, startAt: LocalDateTime, endAt: LocalDateTime): Boolean {
    for (booking in bookings) {
        // Existing booking is fully within the new proposed booking
        // ____[__XX__]___
        //     ^  ^   ^
        //     |  |   \ endAt
        //     |  \ existing booking
        //     | startAt
        if (isAfterOrEqual(booking.startAt, startAt) && isBeforeOrEqual(booking.endAt, endAt)) {
            return true
        }

        // New booking starts within existing booking
        // ____XXX[XX__]___
        // ____XX[XX]X_____
        if (isAfterOrEqual(startAt, booking.startAt) && startAt.isBefore(booking.endAt)) {
            return true
        }

        // New booking ends within existing booking
        // __[__XXX]XX_____
        if (endAt.isAfter(booking.startAt) && isBeforeOrEqual(endAt, booking.endAt)) {
            return true
        }
    }

    return false
}

fun isAfterOrEqual(t1: LocalDateTime, t2: LocalDateTime): Boolean {
    return t1.isAfter(t2) || t1.isEqual(t2)
}

fun isBeforeOrEqual(t1: LocalDateTime, t2: LocalDateTime): Boolean {
    return t1.isBefore(t2) || t1.isEqual(t2)
}