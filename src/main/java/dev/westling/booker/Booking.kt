package dev.westling.booker

import jakarta.persistence.Entity
import jakarta.persistence.GeneratedValue
import jakarta.persistence.Id
import java.time.LocalDateTime

@Entity
class Booking(
    @Id @GeneratedValue var id: Long? = null,
    var room: Int,
    var startAt: LocalDateTime,
    var endAt: LocalDateTime,
    var deletedAt: LocalDateTime? = null,
)