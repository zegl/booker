package dev.westling.booker

import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository
import java.time.LocalDateTime

@Repository
interface BookingRepository : CrudRepository<Booking, Long> {
    fun findByRoomAndStartAtBetweenAndDeletedAtIsNull(
        room: Int, from: LocalDateTime, to: LocalDateTime
    ): Iterable<Booking>

    fun findByDeletedAtIsNull(): Iterable<Booking>
}
